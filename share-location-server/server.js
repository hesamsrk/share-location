const express = require('express')
const db = require('./mongoose')
const app = express()
const port = 4000
const morgan = require('morgan')

app.use(express.json());
const cors = require('cors');
app.use(cors({
    origin: ['http://localhost:3000']
}));
app.use(morgan('tiny'))


app.all('/api/create_new_record', (req, res) => {
    let params = req.body
    if(!params.locationName||!params.locationType||!params.pin){
        throw "check params!"
    }
    db.createNewLocation(params.locationName,params.locationType,params.pin)
    res.json(true)
})

app.all('/api/get_all_records',  (req, res) => {
    db.getAllLocations((err,data)=>{
        if(err){
            res.json(err)
        }else{
            res.json(data)
        }
    })
})


app.all('/api/remove_all_records',(req, res) => {
    db.removeAllLocations(()=>{
        res.json(true)
    })
})



app.listen(port || 4000, () => {
    console.log(`Server listening at http://localhost:${port}`)
})