const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/shareLocation', {useNewUrlParser: true, useUnifiedTopology: true}).then(
    () => console.log('mongoDB connected!')
).catch(
    () => console.log('mongoDB connection error!')
);

const locationSchema = new mongoose.Schema({
    locationName: String,
    locationType: String,
    pin: [Number]
});
const location = mongoose.model('Location', locationSchema);

let getAllLocations = (callback) => {
    location.find((err, data) => {
        if (!err) {
            return callback(undefined, data)
        } else {
            return callback(err, [])
        }
    })
}

let removeAllLocations = (callback) => {
    location.remove({}, callback)
}

let createNewLocation = (locationName, locationType, pin) => {
    const loc = new location({
        locationName, locationType, pin
    });
    loc.save(function (err) {
        if (err) return console.log(err);
        else {
            console.log('new record saved!')
        }
    });
}



module.exports = {getAllLocations, createNewLocation, removeAllLocations}