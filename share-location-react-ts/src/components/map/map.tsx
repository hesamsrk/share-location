import styles from "./map.module.css"
import React, {useEffect} from "react";
import {useQuery} from "react-query";
import {getAllLocations} from "../../api/api";
// @ts-ignore
const leaf = L

interface Record {
    _id: string | number;
    locationName: string;
    locationType: string;
    pin: number[];
}





let myMap:any;
let Map: React.FC<{ setPin: any }> = ({setPin}) => {
    const {data: locations, isError, isLoading} = useQuery('locations', () => getAllLocations())

    useEffect(() => {
        if (locations) {
            let records: Record[] = locations.data
            try {
                // configure map
                if(myMap){
                    myMap.remove()
                }
                myMap =  leaf.map('mapID').setView([51.505, -0.09], 13);
                leaf.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    maxZoom: 18,
                    id: 'mapbox/streets-v11',
                    tileSize: 512,
                    zoomOffset: -1,
                    accessToken: 'pk.eyJ1IjoiaGVzYW1zcmsiLCJhIjoiY2tzNmNhNjI4MHBnbTJvczMyM3lwa2xhYyJ9.uMNJJGOWC6w9RCItvf1y6A'
                }).addTo(myMap);
                // configure saved points
                records.forEach((record: Record) => {
                    let marker = leaf.marker(record.pin).addTo(myMap);
                    marker.bindPopup(`
                    <div style="font-size: 16px;font-weight: bold;text-align: start">
                           ${record.locationType}
                    </div>
                    <div style="font-size: 12px;text-align: start">
                           ${record.locationName}
                    </div>
                `).openPopup();

                })

                // configure click pin
                const mark = leaf.popup();
                const onMapClick = (e: any) => {
                    mark.setLatLng(e.latlng).setContent('selected!').openOn(myMap);
                    setPin([e.latlng.lat, e.latlng.lng])

                }
                myMap.on('click', onMapClick);
            } catch (e) {
                // This section of code throws exceptions only during the development
            }

        }
    }, [ locations])


    if (isLoading) {
        return <span>Loading...</span>
    }

    if (isError) {
        return <span>Error!</span>
    }

    return (
        <>
            <div className={styles.hint}>
                Select a location on the map:
            </div>
            <div id="mapID" className={styles.map}/>
        </>
    );
};

export default Map;