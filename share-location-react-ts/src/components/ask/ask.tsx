import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Slide from '@material-ui/core/Slide';
import {useState, forwardRef, useEffect} from 'react';
import * as React from "react";
import {TransitionProps} from '@material-ui/core/transitions/transition';
import {useDispatch} from "react-redux";
import {addHelper} from '../../redux/slices/globalHelpers'
interface Action {
    title: string,
    handle: () => void
}


const Transition: React.ComponentType<TransitionProps & { children?: React.ReactElement<any, any> }> = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


let greenDef: (setter: any) => Action = (setter) => {
    return {
        title: 'Agree',
        handle: () => {
            setter(false)
        }
    }
}
let redDef: (setter: any) => Action = (setter) => {
    return {
        title: 'Disagree',
        handle: () => {
            setter(false)
        }
    }
}


const AskModule:React.FC =  () => {
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState("Are you sure?")
    const [green, setGreen] = useState(greenDef(setOpen))
    const [red, setRed] = useState(redDef(setOpen))
    const dispatch = useDispatch()

    useEffect(()=>{
        const ask = (message: string, green: Action, red: Action) => {

            setOpen(true);
            if (message) {
                setMessage(message)
            }
            if (green) {
                let gr = greenDef(setOpen)
                setGreen({
                    title: green.title ? green.title : gr.title,
                    handle: green.handle ? () => {
                        green.handle()
                        setOpen(false)
                    } : gr.handle,
                })
            }
            if (red) {
                let rd = redDef(setOpen)
                setRed({
                    title: red.title ? red.title : rd.title,
                    handle: red.handle ? () => {
                        red.handle()
                        setOpen(false)
                    } : rd.handle,
                })
            }

        }
        dispatch(addHelper({ask}))
    },[dispatch])


    return (
        <Dialog
            style={{zIndex: 1305}}
            onClose={(event, reason) => {
                if(reason==="backdropClick"){
                    setOpen(false)
                }
            }}
            open={open}
            TransitionComponent={Transition}
            keepMounted>
            <DialogContent>
                <DialogContentText>
                    {message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={red.handle} color="primary">
                    {red.title}
                </Button>
                <Button onClick={green.handle} color="primary">
                    {green.title}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default AskModule
