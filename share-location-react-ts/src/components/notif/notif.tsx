import Snackbar from "@material-ui/core/Snackbar";
import React, {useState} from "react";
import MuiAlert from '@material-ui/lab/Alert';
import {useDispatch} from "react-redux";
import {addHelper} from '../../redux/slices/globalHelpers'


function Alert(props: any) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Notif: React.FC = () => {
    const [alertOpen, setAlertOpen] = useState(false);
    const [alertType, setAlertType] = useState<string>('success');
    const [alertMessage, setAlertMessage] = useState('');
    const dispatch = useDispatch()

    const alert = (message: string, type: string) => {
        setAlertOpen(true);
        setAlertType(type)
        setAlertMessage(message)
    }
    dispatch(addHelper({alert}))

    const handleClose = (event: any, reason: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setAlertOpen(false);
    };
    return (<Snackbar open={alertOpen} autoHideDuration={3000} onClose={handleClose}
                      anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
        <Alert onClose={handleClose} severity={alertType || 'info'}>
            {alertMessage}
        </Alert>
    </Snackbar>);
};

export default Notif