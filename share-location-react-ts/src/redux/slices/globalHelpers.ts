import {createSlice} from '@reduxjs/toolkit'

export const globalHelpersSlice = createSlice({
    name: 'globalHelpers',
    initialState: {
        helpers: {}
    },
    reducers: {
        addHelper: (state, action) => {
            let payload:object = action.payload
            let key:string = Object.keys(payload)[0]
            // @ts-ignore
            state.helpers[key] = payload[key]
        }
    },
})

// Action creators are generated for each case reducer function
export const {addHelper} = globalHelpersSlice.actions

export default globalHelpersSlice.reducer