import {createSlice} from '@reduxjs/toolkit'

export const shareLocationFormSlice = createSlice({
    name: 'shareLocationForm',
    initialState: {
        locationName: '',
        locationType: 'undefined',
        pin: undefined
    },
    reducers: {
        setLocationNameReducer: (state, action) => {
            state.locationName = action.payload
        },
        setLocationTypeReducer: (state, action) => {
            state.locationType = action.payload
        },
        setPinReducer: (state, action) => {
            state.pin = action.payload
        }
    },
})

// Action creators are generated for each case reducer function
export const {setLocationNameReducer, setLocationTypeReducer, setPinReducer} = shareLocationFormSlice.actions

export default shareLocationFormSlice.reducer