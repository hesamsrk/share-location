import {configureStore} from '@reduxjs/toolkit'
import shareLocationFormReducer from "./slices/shareLocationForm";
import globalHelpersReducer from "./slices/globalHelpers";


export default configureStore({
    reducer: {
        shareLocationForm: shareLocationFormReducer,
        globalHelpers: globalHelpersReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
})

