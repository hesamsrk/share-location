import styles from './mainPage.module.css'
import InputCus from './../../components/inputCus/inputCus'
import SelectCus from './../../components/selectCus/selectCus'
import Map from "./../../components/map/map"
import React from "react";
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import WorkOutlineOutlinedIcon from '@material-ui/icons/WorkOutlineOutlined';
import SchoolOutlinedIcon from '@material-ui/icons/SchoolOutlined';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import {Button} from '@material-ui/core'
import {sendLocation, removeAllLocations} from '../../api/api'
import {useDispatch, useSelector} from "react-redux";
import {setLocationNameReducer, setLocationTypeReducer, setPinReducer} from '../../redux/slices/shareLocationForm';
import {useQueryClient} from "react-query";

const items = [
    {value: "workplace", title: 'Work Place', icon: WorkOutlineOutlinedIcon},
    {value: "home", title: 'Home', icon: HomeOutlinedIcon},
    {value: "school", title: 'School', icon: SchoolOutlinedIcon},
    {value: "other", title: 'Other', icon: AddOutlinedIcon},
]


let MainPage: React.FC = () => {
    const dispatch = useDispatch()
    const queryClient = useQueryClient()
    // @ts-ignore
    const {pin, locationName, locationType} = useSelector(state => state.shareLocationForm)
    // @ts-ignore
    const {helpers} = useSelector(state => state.globalHelpers)
    const setPin = (value: number[] | undefined) => dispatch(setPinReducer(value))
    const setLocationName = (value: string) => dispatch(setLocationNameReducer(value))
    const setLocationType = (value: string) => dispatch(setLocationTypeReducer(value))


    const confirm = () => {
        if (locationType.length !== 0 && locationName.length !== 'undefined' && !!pin) {
            sendLocation(locationName, locationType, pin).then(
                () => {
                    helpers.alert("Location sent!", 'success')
                    setPin(undefined)
                    setLocationType('undefined')
                    setLocationName('')
                    queryClient.invalidateQueries('locations')
                }
            ).catch((e) => {
                console.error(e)
                helpers.alert("server error!", 'error')
            })
        } else {
            helpers.alert("Fill the fields and select the location on the map!", 'error')
        }
    }

    const removeAll = () => {
        let ok = () => {
            removeAllLocations().then(
                () => {
                    helpers.alert('Done!', 'success')
                    queryClient.invalidateQueries('locations')
                }
            ).catch(
                () => {
                    helpers.alert('Remove all records failed!', 'error')

                }
            )
        }

        helpers.ask("Are you sure? All location tags will be removed.", {title: "Yes I'm sure",handle: ok}, {title: 'Cancel'})

    }


    return (
        <div className={styles.container}>
            <div className={styles.innerContainer}>
                <div className={`title`}>
                    Share Address
                </div>
                <div className={`paper ${styles.card}`}>
                    <div className={styles.row}>
                        <div className={styles.col6}>
                            <InputCus
                                onChange={(e) => setLocationName(e.target.value)}
                                value={locationName}
                                label={'Location Name'}
                                hint={'example: My aunt\'s place'}
                            />
                        </div>
                        <div className={styles.col6}>
                            <SelectCus label={'Location Type'} value={locationType}
                                       onChange={(e) => setLocationType(e.target.value)} items={items}/>
                        </div>
                    </div>
                    <div className={styles.row}>
                        <Map setPin={setPin}/>
                    </div>
                </div>
                <div className={[styles.buttonRow, 'mt-1'].join(' ')}>
                    <div className='w-50 pr-1'>
                        <Button className='w-100' variant={"contained"} color={"secondary"} onClick={removeAll}>Remove
                            All</Button>
                    </div>
                    <div className='w-50 pl-1'>
                        <Button className='w-100' variant={"contained"} color={"primary"}
                                onClick={confirm}>Confirm</Button>
                    </div>

                </div>
            </div>
        </div>
    )
}


export default MainPage;

