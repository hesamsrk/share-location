import axios from "axios";

let host_server_url = 'http://localhost:4000'
let url = (api_name:string)=>{
    return `${host_server_url}/api/${api_name}`
}
let sendLocation = (locationName:string,locationType:string,pin:number[])=>{
    return axios.post(url('create_new_record'), {locationName,locationType,pin})
}


let getAllLocations = ()=>{
    return axios.post(url('get_all_records'))
}


let removeAllLocations = ()=>{
    return axios.post(url('remove_all_records'))
}



export {sendLocation,getAllLocations,removeAllLocations}
