<!--lint disable no-literal-urls-->
# Intoductions

[share-location-react-ts](./share-location-react-ts) folder contains the Reactjs web application written in Typescript.
I used the following libraries/frameworks during implementaton.
* [React-Query](https://react-query.tanstack.com/)
* [Reduxjs](https://redux.js.org/)
* [Material-UI](https://material-ui.com/)
* [leafletjs](https://leafletjs.com/)

[share-location-server](./share-location-server) folder contains a nodejs/express web server used to save and load data .

# Installaion and Run

In order to run the [Server](./share-location-server/server.js) fisrt you need To download and install `mongoDB` on your machine using [This]() link.


Then Run the following commands when you are in [share-location-react-ts](./share-location-react-ts):
```console
$ npm install
$ npm start
```

Then Run the following commands when you are in [share-location-server](./share-location-server):
* ( you have to install [nodejs](https://nodejs.org) on your device to run this code )
```console
$ npm install
$ node ./server.js
```

Now open [http://localhost:3000](http://localhost:3000) on your browser.
